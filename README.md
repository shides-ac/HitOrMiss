## HitOrMiss  
These repository was created to maintain the project for "Metodi Matematici e Statistici" course.  
  
### About
+ You can read more about Monte Carlo "Hit or Miss" method at its [My Github Page](https://anto4c.github.io/HitOrMiss/)  
+ There a deployed version at the following link [Monte Carlo - Hit Or Miss](http://anto4c.altervista.org/mms/)  
 
### License
[![Licenza Creative Commons](https://raw.githubusercontent.com/Anto4C/HitOrMiss/master/images/license.png)](http://creativecommons.org/licenses/by-sa/4.0/)  
[Hit Or Miss](https://anto4c.github.io/HitOrMiss/) by [Antonio Caragliano](https://www.github.com/Anto4C) is licensed under a [Creative Commons Attribution-ShareAlike 4.0 International License](http://creativecommons.org/licenses/by-sa/4.0/).
