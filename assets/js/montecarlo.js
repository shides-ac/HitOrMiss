function MonteCarlo(n, pointsArray){
	if(!n)
		throw new Error("MonteCarlo - no params or error in params");
	
	var randomPoints = pointsArray,
		pointsNumber = n,
		hits, AS, ASInterval, deviance;

	var genNewPoints = function(){
		//cleaning the array
		randomPoints.length = 0;
		for(var i=0; i<pointsNumber; i++)
			randomPoints.push([
				Math.random(),
				Math.random()
			]);

		/* for testing:
			randomPoints should have n length
		
		console.log("generateRandomPoint: printing randomPoints.length");
		console.log(randomPoints.length);
		console.log("generateRandomPoint: printing the array");
		console.log(randomPoints);
		*/
	};

	var hitsCounter = function(){
		hits = 0;
		for(var i in randomPoints){
			var point = randomPoints[i],
				num1 = Math.sqrt(1-Math.pow(point[0], 2)), //sqrt(1 − U^2)
				num2 = point[1]; //U'

			if(num1 >= num2)
				hits++;
		}
	};

	var estimateAS = function(){
		AS = (hits/randomPoints.length); //hits number / point number
	};

	var estimateDeviance = function(){
		//σ = sqrt(AS(1 − AS)/N)
		if(AS){
			deviance = (Math.sqrt((AS*(1-AS))/randomPoints.length));
			console.log("priting the deviance for: " + randomPoints.length);
			console.log(deviance);
		}
	};

	var estimateASInterval = function(){
		ASInterval = {};
		ASInterval.min = (AS - (3*deviance));
		ASInterval.max = (AS + (3*deviance));
	};

	var calculatePi = function(){
		return (4*AS);
	};

	var hitOrMiss = function(){
		genNewPoints();
		hitsCounter();
		estimateAS();
		estimateDeviance();
		estimateASInterval();
	}

	var getPoints = function(){
		return randomPoints;
	};

	var setPointNumber = function(n){
		pointsNumber = n;
	};

	var getHits = function(){
		return hits;
	};

	var getAS = function(){
		return AS;
	};

	var getASInterval = function(){
		return ASInterval;
	};

	var getDeviance = function(){
		return deviance;
	};

	return {
		genNewPoints: genNewPoints,
		setPointNumber: setPointNumber,
		getHits: getHits,
		getPoints: getPoints,
		getAS: getAS,
		getASInterval: getASInterval,
		getDeviance: getDeviance,
		hitOrMiss: hitOrMiss,
		calculatePi: calculatePi,
	};
}