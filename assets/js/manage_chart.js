$(document).ready(function(){

  var myPoints = [], montecarlo, pointsNumber,
      myPlot, lineChart;

  var functionPlotOptions = {
    target: '#demo_hitormiss',
    xAxis: {
      domain: [0,1.5],
      label: 'X',
    },
    yAxis: {
      domain: [0,1],
      label: 'Y',
    },
    disableZoom: true,
    grid: true,
    //adding arc function and points
    data: [{
        fn: 'sqrt(1-x^2)'
      },{
      points: [],
      fnType: 'points',
      graphType: 'scatter'
    }],
    //setting tip view 
    tip: {
      xLine: true,    // dashed line parallel to y = 0
      yLine: true,    // dashed line parallel to x = 0
      renderer: function (x, y, index) {
        // the returning value will be shown in the tip
      }
    },
  };

  //Binding "enter" press to "execute" button click
  $('#inputNumber').keypress(function (event){
    if(event.which == 13){
      event.preventDefault();
      $('#executeMC').click();
    }
  });

  /* 
   * Binding "executeMC" button 'click' event to a function that
   * runs montecarlo's hit or miss algorithm an then show a demo
   * on a function plotter
   */
  $("#executeMC").bind('click', function(){
    var nHits, deviance, AS, ASInterval;

    pointsNumber = parseInt($('#inputNumber').val());//getting input for point number

    //Error check
    if(!pointsNumber) //user error
      alert("Non hai inserito il numero di coppie da creare");
    else if(pointsNumber>50000 || pointsNumber<0){
      alert("Hai inserito un numero di coppie non valido (0<x<50000)");
    } else { //everything ok

      montecarlo = new MonteCarlo(pointsNumber, myPoints); //init MonteCarlo object
      montecarlo.hitOrMiss(); //execute hitormiss

      //setting view values
      nHits = montecarlo.getHits();
      deviance = montecarlo.getDeviance();
      AS = montecarlo.getAS();
      ASInterval = montecarlo.getASInterval();

      $('#hitsNumber').html(""+nHits);
      $('#deviance').html(""+deviance);
      $('#areaAS').html(""+AS);
      $('#confidenceInterval').html("["+ASInterval.min+"; "+ASInterval.max+" ]");

      //plotting the graphics
      functionPlotOptions.data[1].points = myPoints; //swap points array

      myPlot = functionPlot(functionPlotOptions);
    }

  });

  //plot a radius
  myPlot = functionPlot(functionPlotOptions);

  //defining a function to draw a chartis-js chart for PI approximation
  var pending;
  var drawLineChart = function (){
    pending = true; //useful to avoid chart override

    //drawing a chart for n = 0 to 500000 points using a 50000 steps
    var chartLabels = [], dataset = [];
    dataset[0] = []; //will contains pi from montecarlo
    dataset[1] = []; //will contains fixed pi
    dataset[2] = []; //will container the as interval 
    dataset[3] = [];
    var montecarlos = [], points = []; //array of MonteCarlo object, each one with a different number of point
    var executionMin=100,
        executionMax=0; //useful to chart y-axis range

    for(var i=10000; i<=1000000; i*=2){
      var pi, deviance;

      chartLabels.push(""+i); //pushing label 

      montecarlos[i] = new MonteCarlo(i, []); //gen montecarlo for i 

      montecarlos[i].hitOrMiss();
      var pi = montecarlos[i].calculatePi(), //getting estimated PI
          deviance = montecarlos[i].getDeviance(), //getting deviance
          asInterval = montecarlos[i].getASInterval();

      console.log(""+montecarlos[i].getPoints().length+" points -> " + pi);

      dataset[0].push(pi); //pushing the estimated pi
      dataset[1].push(3.14159); //pushing a fixed pi
      dataset[2].push([pi-deviance, pi+deviance]); //pushing errorbar value 
      //dataset[3].push([3.14159-deviance, 3.14159+deviance]);

      if((pi-deviance)<executionMin)
        executionMin=(pi-deviance);
      if ((pi+deviance)>executionMax)
        executionMax=(pi+deviance);
    }

    lineChart = $('#approx_pi_linechart').highcharts({
      title: {
        text: "Grafico n->500.000",
      },
      xAxis: {
        categories: chartLabels
      },
      yAxis: {
        min: executionMin,
        max: executionMax,
        tickInterval: 0.01,
      },
      legend: {
        layout: 'vertical',
        align: 'right',
        verticalAlign: 'middle',
        borderWidth: 0
      },
      series: [{
        type: 'line',
        name: 'Monte Carlo',
        data: dataset[0],
      },{
        type: 'errorbar',
        name: 'Errorbar',
        showInLegend: true,
        color: '#FF0000',
        whiskerColor:'#FFFFFF',
        whiskerLength:15,
        whiskerWidth: 2,
        stemWidth: 2, 
        data: dataset[2],
      }, {
        type: 'line',
        dashStyle: 'ShortDot',
        name: 'PI',
        data: dataset[1],
      }],
    });
    pending = false;
    /*
    lineChart = new Chartist.Line('#approx_pi_linechart', {
        labels: chartLabels,
        series: [dataset[0], dataset[1]]
      }, {
        fullWidth: true,
        chartPadding: {
          right: 40
        }
    });
    // Let's put a sequence number aside so we can use it in the event callbacks
    var seq = 0,
      delays = 40,
      durations = 500;

    // Once the chart is fully created we reset the sequence
    lineChart.on('created', function() {
      seq = 0;
    });

    // On each drawn element by Chartist we use the Chartist.Svg API to trigger SMIL animations
    lineChart.on('draw', function(data) {
      seq++;

      if(data.type === 'line') {
        // If the drawn element is a line we do a simple opacity fade in. This could also be achieved using CSS3 animations.
        data.element.animate({
          opacity: {
            // The delay when we like to start the animation
            begin: seq * delays + 1000,
            // Duration of the animation
            dur: durations,
            // The value where the animation should start
            from: 0,
            // The value where it should end
            to: 1
          }
        });
      } else if(data.type === 'label' && data.axis === 'x') {
        data.element.animate({
          y: {
            begin: seq * delays,
            dur: durations,
            from: data.y + 100,
            to: data.y,
            // We can specify an easing function from Chartist.Svg.Easing
            easing: 'easeOutQuart'
          }
        });
      } else if(data.type === 'label' && data.axis === 'y') {
        data.element.animate({
          x: {
            begin: seq * delays,
            dur: durations,
            from: data.x - 100,
            to: data.x,
            easing: 'easeOutQuart'
          }
        });
      } else if(data.type === 'point') {
        data.element.animate({
          x1: {
            begin: seq * delays,
            dur: durations,
            from: data.x - 10,
            to: data.x,
            easing: 'easeOutQuart'
          },
          x2: {
            begin: seq * delays,
            dur: durations,
            from: data.x - 10,
            to: data.x,
            easing: 'easeOutQuart'
          },
          opacity: {
            begin: seq * delays,
            dur: durations,
            from: 0,
            to: 1,
            easing: 'easeOutQuart'
          }
        });
      } else if(data.type === 'grid') {
        // Using data.axis we get x or y which we can use to construct our animation definition objects
        var pos1Animation = {
          begin: seq * delays,
          dur: durations,
          from: data[data.axis.units.pos + '1'] - 30,
          to: data[data.axis.units.pos + '1'],
          easing: 'easeOutQuart'
        };

        var pos2Animation = {
          begin: seq * delays,
          dur: durations,
          from: data[data.axis.units.pos + '2'] - 100,
          to: data[data.axis.units.pos + '2'],
          easing: 'easeOutQuart'
        };

        var animations = {};
        animations[data.axis.units.pos + '1'] = pos1Animation;
        animations[data.axis.units.pos + '2'] = pos2Animation;
        animations['opacity'] = {
          begin: seq * delays,
          dur: durations,
          from: 0,
          to: 1,
          easing: 'easeOutQuart'
        };

        data.element.animate(animations);
      }
    });
    pending = false;
    */
  };

  //binding "new execution" button's click event to a function that call drawLineChart()
  $('#new_chart').bind('click', function(){
    if(!pending)
      drawLineChart();
    else
      alert("Attendi il completamento del grafico per poterne creare uno nuovo");
  });

  lineChart = $('#approx_pi_linechart').highcharts({
      title: {
        text: "Grafico n->500.000",
      },
      xAxis: {
      },
      yAxis: {
        min: 3.12,
        max: 3.16,
        tickInterval: 0.01,
      },
      legend: {
        layout: 'vertical',
        align: 'right',
        verticalAlign: 'middle',
        borderWidth: 0
      },
      series: [{
        type: 'line',
        dashStyle: 'ShortDot',
        name: 'PI',
        data: [Math.PI,Math.PI,Math.PI,Math.PI,Math.PI,Math.PI,Math.PI,Math.PI,Math.PI],
      }],
    });
});